<?php

/**
 * Contains payment_alipay_i_PaymentMethodController.
 */

/**
 * A PayPal Payments Standard payment method.
 */
class payment_alipay_i_PaymentMethodController extends PaymentMethodController {

  public $controller_data_defaults = array(
    'partner' => '',
    'akey' => '',
    'seller_email' => '',
  );

  public $payment_method_configuration_form_elements_callback = 'payment_alipay_i_payment_method_configuration_form_elements';

  function __construct() {
    $this->title = 'Alipay Instant';
  }

  /**
   * Implements PaymentMethodController::validate().
   */
  function validate(Payment $payment, PaymentMethod $payment_method, $strict) {
  }

  /**
   * Implements PaymentMethodController::execute().
   */
  function execute(Payment $payment) {
    entity_save('payment', $payment);
    $_SESSION['payment_alipay_i_pid'] = $payment->pid;
    drupal_goto('payment_alipay_i/redirect/' . $payment->pid);
  }

}
